# IOC to control TDK Lambda Z series power supplies

## Used modules

*   [tdklambdaz](https://gitlab.esss.lu.se/e3/wrappers/ps/e3-tdklambdaz.git)


## Controlled devices

*    TS2-010CRM:Cryo-PSU-022
    *   TS2-010CRM:Cryo-PSU-023
    *   TS2-010CRM:Cryo-PSU-030
    *   TS2-010CRM:Cryo-PSU-031
    *   TS2-010CRM:Cryo-PSU-032
    *   TS2-010CRM:Cryo-PSU-033
