#
# Module: essioc
#
require essioc

#
# Module: tdklambdaz
#
require tdklambdaz


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${tdklambdaz_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: TS2-010CRM:Cryo-PSU-022
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_master.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-022, IPADDR = ts2-cryo-tdklambda2.tn.esss.lu.se, RS485_ADDR = 1")

#
# Device: TS2-010CRM:Cryo-PSU-023
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-023, MASTER = TS2-010CRM:Cryo-PSU-022, RS485_ADDR = 2")

#
# Device: TS2-010CRM:Cryo-PSU-030
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-030, MASTER = TS2-010CRM:Cryo-PSU-022, RS485_ADDR = 3")

#
# Device: TS2-010CRM:Cryo-PSU-031
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-031, MASTER = TS2-010CRM:Cryo-PSU-022, RS485_ADDR = 4")

#
# Device: TS2-010CRM:Cryo-PSU-032
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-032, MASTER = TS2-010CRM:Cryo-PSU-022, RS485_ADDR = 5")

#
# Device: TS2-010CRM:Cryo-PSU-033
# Module: tdklambdaz
#
iocshLoad("${tdklambdaz_DIR}/tdklambdaz_slave.iocsh", "DEVICENAME = TS2-010CRM:Cryo-PSU-033, MASTER = TS2-010CRM:Cryo-PSU-022, RS485_ADDR = 6")
